import { Col, Divider, Row, Typography } from "antd";
import React, { PureComponent } from "react";
import { Link } from "react-router-dom";

const { Title, Paragraph, Text } = Typography;

export default class CPrivacy extends PureComponent {
  render() {
    return (
      <>
        <Row>
          <Col>
            <Title>Privacy</Title>
          </Col>
        </Row>

        <Row>
          <Col>
            <Divider />
          </Col>
        </Row>

        <Row>
          <Col>
            <Title level={2}>About our goals</Title>
            <Paragraph>
              At put.re we aim to create a convenient balance between privacy
              and usability. First of all a few answers to most common
              questions, geeks can read more about the security model later.
            </Paragraph>
            <Paragraph>
              We do not store any identifiers, including the most common ones:
            </Paragraph>
            <ul>
              <li>IP address</li>
              <li>Other IP address derived metadata like the country</li>
              <li>Fingerprints</li>
              <li>Any browser settings or headers in general</li>
            </ul>
            <Paragraph>
              Additionally you may have noticed the lack of ads or trackers on
              the website!
            </Paragraph>
          </Col>
        </Row>

        <Row>
          <Col>
            <Title level={2}>File processing</Title>
            <Paragraph>
              All files are MD5 perfect, which means you get exactly what you
              give us. Unless a lot of other services we do not apply unwanted
              compression or do shenanigans with your files. Due to this we also
              do not process your file in any way besides encryption and
              decryption routines.
            </Paragraph>
          </Col>
        </Row>

        <Row>
          <Col>
            <Title level={2}>Encryption</Title>
            <Paragraph>
              All files are encrypted,{" "}
              <Text strong>without us being able to read any file</Text>. As
              mentioned earlier put.re is a convenience crossover between
              privacy and usability. Remember to never trust anybody about
              security, not even this page! If you have above average security
              demands you should make use of a service that leverages encryption
              on the browserside and not the server side. To be clear: put.re is
              using server side processing, to be able to provide an API that
              can be used with image capturing tools and other third party
              software.
            </Paragraph>
          </Col>
        </Row>

        <Row>
          <Col>
            <Title level={2}>Attack vector</Title>
            <Paragraph>You have to trust us that:</Paragraph>
            <ul>
              <li>We really do what we just described</li>
              <li>Do not keep any logs that could give away the file id</li>
            </ul>
          </Col>
        </Row>

        <Row>
          <Col>
            <Title level={2}>Contact</Title>
            <Paragraph>
              For issues that are <Text strong>not</Text> a{" "}
              <Link to="/removal-request">removal request</Link> please reach us
              under <a href="mailto:contact@put.re">contact@put.re</a>. We
              support <a href="https://s.put.re/ZaWzoiZ7.re.asc">PGP</a> for
              sensitive messages.
            </Paragraph>
          </Col>
        </Row>
      </>
    );
  }
}
