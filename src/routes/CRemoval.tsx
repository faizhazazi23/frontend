import {
  Button,
  Col,
  Divider,
  Form,
  Input,
  message,
  Row,
  Typography
} from "antd";
import { FormComponentProps } from "antd/es/form";
import axios from "axios";
import React, { Component } from "react";

import extractMatches from "../utils/extractMatches";

interface CRemovalProps extends FormComponentProps {
  urls?: string;
  reason?: string;
  validateOnLoad?: boolean;
}

interface CRemovalState {
  sent: boolean;
}

class CRemoval extends Component<CRemovalProps> {
  readonly state: CRemovalState = { sent: false };

  componentDidMount() {
    if (this.props.validateOnLoad) {
      this.props.form.validateFields();
    }
  }

  private handleSubmit = async (
    e: React.FormEvent<HTMLFormElement>
  ): Promise<void> => {
    e.preventDefault();

    this.props.form.validateFields(async (err, values) => {
      if (err) {
        return;
      }

      try {
        await axios.post("https://api.put.re/removal", {
          files: values.urls,
          reason: values.reason
        });

        this.setState({ sent: true });
        message.info("Removal request sent successfully");
      } catch (_) {
        message.error("Request failed! Please try again later.");
      }
    });
  };

  render() {
    const { getFieldDecorator } = this.props.form;

    return (
      <>
        <Row>
          <Col>
            <Typography.Title>Removal Request</Typography.Title>
          </Col>
        </Row>

        <Row>
          <Col>
            <Divider />
          </Col>
        </Row>

        <Row>
          <Col>
            <Form onSubmit={this.handleSubmit}>
              <Form.Item>
                List of files to be deleted, separated by commas.
                {getFieldDecorator("urls", {
                  initialValue: this.props.urls,
                  validateTrigger: "onLoad",
                  rules: [
                    {
                      required: true,
                      message: "We require valid URLs",
                      validator: async (_, value, callback) => {
                        const matches = await extractMatches(value);

                        if (matches.length < 1) {
                          callback(new Error());
                          return;
                        }

                        callback();
                      }
                    },
                    {
                      required: true,
                      message: "Looks like you already have your deletion url!",
                      validator: (_, value, callback) => {
                        if (value.indexOf("delete") > -1) {
                          callback(new Error());
                          return;
                        }

                        callback();
                      }
                    }
                  ]
                })(<Input placeholder="URL1, URL2, URL3, ..." />)}
              </Form.Item>

              <Form.Item>
                Why should these files be removed?
                {getFieldDecorator("reason", {
                  initialValue: this.props.reason,
                  rules: [
                    {
                      required: true,
                      message: "We do not accept requests without valid reason"
                    }
                  ]
                })(
                  <Input.TextArea
                    placeholder="Without a detailed reason or proof of ownership we will ignore your request *!*"
                    rows={4}
                  />
                )}
              </Form.Item>

              <Form.Item>
                <Button
                  disabled={this.state.sent}
                  htmlType="submit"
                  type="primary"
                >
                  Submit
                </Button>
              </Form.Item>
            </Form>
          </Col>
        </Row>
      </>
    );
  }
}

export default Form.create<CRemovalProps>({ name: "removal" })(CRemoval);
