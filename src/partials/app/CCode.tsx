import "codemirror/lib/codemirror.css";
import "codemirror/theme/material.css";
import "codemirror/mode/javascript/javascript.js";
import "./CCode.css";

import React, { PureComponent } from "react";
import { UnControlled as CodeMirror } from "react-codemirror2";

interface CCodeProps {
  mode: string;
  value: string;
}

export default class CCode extends PureComponent<CCodeProps> {
  render() {
    return (
      <CodeMirror
        options={{
          mode: this.props.mode,
          theme: "material",
          lineNumbers: true,
          readOnly: true
        }}
        value={this.props.value}
        onChange={() => {}}
      />
    );
  }
}
