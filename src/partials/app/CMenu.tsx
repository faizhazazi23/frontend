import { Menu } from "antd";
import { inject, observer } from "mobx-react";
import React, { Component } from "react";
import { RouteComponentProps, withRouter } from "react-router";
import { Link } from "react-router-dom";

import { IStores } from "../../CApp";

interface CMenuProps extends Partial<RouteComponentProps<any>> {
  mobile?: boolean;
  stores?: IStores;
}

@inject("stores")
@observer
class CMenu extends Component<CMenuProps> {
  private store = this.props.stores!.album;

  private get items(): JSX.Element[] {
    const items: { title: string; url: string }[] = [];

    if (!this.props.mobile) {
      items.push(
        { title: "Terms", url: "/terms-of-service" },
        { title: "Status", url: "/status" },
        { title: "API", url: "/api-docs" },
        { title: "Privacy", url: "/privacy" },
        { title: "Features", url: "/features" }
      );
    }

    items.push(
      { title: "Removal", url: "/removal-request" },
      { title: "Album", url: `/album/${this.store.albumId}` }
    );

    return items.map(value => (
      <Menu.Item key={value.url} style={{ float: "right" }}>
        <Link to={value.url}>{value.title}</Link>
      </Menu.Item>
    ));
  }

  render() {
    // This is a compiler hack, do not remove it
    const path = this.props.location!.pathname;

    return (
      <Menu mode="horizontal" selectedKeys={[path]}>
        <Menu.Item
          key="/"
          style={{
            float: "left",
            fontWeight: "bold",
            fontSize: "14px"
          }}
        >
          <Link style={{ color: "#3889EA" }} to="/">
            PUT.RE
          </Link>
        </Menu.Item>

        {this.items}
      </Menu>
    );
  }
}

export default withRouter<any, any>(CMenu);
