import { Button, Col, Input, Modal, Row } from "antd";
import { inject, observer } from "mobx-react";
import React, { Component } from "react";

import { IStores } from "../../CApp";

interface CNavigationProps {
  stores?: IStores;
}

@inject("stores")
@observer
export default class CNavigation extends Component<CNavigationProps> {
  private readonly store = this.props.stores!.album;

  render() {
    if (!this.store.loaded) {
      return (
        <>
          <Row>
            <Col style={{ float: "left" }}>
              <Button
                disabled={this.store.isCreated}
                icon="plus"
                onClick={() => this.store.createAlbum()}
              >
                Create
              </Button>
            </Col>
          </Row>
        </>
      );
    }

    return (
      <>
        <Modal
          title="Secret"
          visible={this.store.modals[0]}
          onCancel={() => this.store.toggleModal(0)}
          onOk={() => this.store.toggleModal(0)}
        >
          <Input.Password
            allowClear
            placeholder="input secret"
            value={this.store.secret}
            onChange={e => (this.store.secret = e.target.value)}
          />
        </Modal>

        <Modal
          confirmLoading={this.store.isProcessing}
          title="Add files"
          visible={this.store.modals[1]}
          onCancel={() => this.store.toggleModal(1)}
          onOk={() =>
            this.store.addEntriesByList(this.store.inputFile, () => {
              this.store.toggleModal(1);
              this.store.inputFile = "";
            })
          }
        >
          <Input.TextArea
            autosize={{ minRows: 4, maxRows: 12 }}
            placeholder="One (1) URL per line"
            value={this.store.inputFile}
            onChange={e => (this.store.inputFile = e.target.value)}
          />
        </Modal>

        <Row>
          <Col style={{ float: "left" }}>
            {this.store.canEdit ? (
              <Button icon="lock" onClick={() => (this.store.secret = "")}>
                Lock Album
              </Button>
            ) : (
              <Button icon="unlock" onClick={() => this.store.toggleModal(0)}>
                Unlock Album
              </Button>
            )}
          </Col>

          {this.store.canEdit && (
            <>
              <Col style={{ float: "left", padding: "0px 0px 0px 10px" }}>
                <Button
                  icon="file-add"
                  onClick={() => this.store.toggleModal(1)}
                >
                  Add file
                </Button>
              </Col>

              <Col style={{ float: "left", padding: "0px 0px 0px 10px" }}>
                <Button
                  disabled={!this.store.dirty}
                  icon="upload"
                  onClick={() => this.store.sync()}
                >
                  Save
                </Button>
              </Col>

              {this.store.view === "grid" && (
                <Col style={{ float: "left", padding: "0px 0px 0px 10px" }}>
                  <Button
                    disabled={this.store.selectedEntries.length < 1}
                    icon="delete"
                    onClick={() => this.store.deleteSelectedEntries()}
                  >
                    Delete selected ({this.store.selectedEntries.length})
                  </Button>
                </Col>
              )}
            </>
          )}

          <Col style={{ float: "right" }}>
            <Button icon="layout" onClick={() => this.store.toggleView()}>
              {this.store.view === "grid" ? "Light view" : "Grid view"}
            </Button>
          </Col>
        </Row>
      </>
    );
  }
}
