const http = require("http");
const fs = require("fs");
const serveStatic = require("serve-static");
const puppeteer = require("puppeteer");

const serve = serveStatic("build", { index: ["index.html", "index.htm"] });

const server = http.createServer(function onRequest(req, res) {
  if (req.url === "/purgecss") {
    req.url = "/index.html";
  }

  serve(req, res, () => {
    res.writeHead(200);
    res.end("");
  });
});

server.listen(8080);

(async () => {
  const browser = await puppeteer.launch({
    args: ["--no-sandbox", "--disable-setuid-sandbox"]
  });
  const page = await browser.newPage();

  await page.goto("http://127.0.0.1:8080/purgecss", {
    waitUntil: "networkidle0"
  });

  const viewports = [
    [320, 568], // iPhone 5
    [375, 667], // iPhone 6/7/8
    [414, 896], // iPhone XR
    [768, 1024], // iPad Mini
    [1024, 1366], // iPad Pro
    [1920, 1080], // Full HD
    [2560, 1440], // Quad HD
    [3840, 2160] // Ultra HD
  ];

  for (let index = 0; index < viewports.length; index++) {
    const el = viewports[index];

    await page.setViewport({
      width: el[0],
      height: el[1]
    });

    fs.writeFileSync(
      `purgecss/rendered-${el[0]}_${el[1]}.html`,
      await page.content()
    );
  }

  await browser.close();

  server.close();
})();
